class Timer
  attr_accessor :seconds

  def initialize(seconds=0)
    @seconds = seconds
  end

  def prepend_zero(value)
    displayed_value = value < 10 ? "0" << value.to_s : value.to_s
  end

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds % 3600) / 60
    seconds = (@seconds % 3600) % 60

    displayed_hours = prepend_zero(hours)
    displayed_minutes = prepend_zero(minutes)
    displayed_seconds = prepend_zero(seconds)
    displayed_time = "#{displayed_hours}:#{displayed_minutes}:#{displayed_seconds}"
  end
end
