class Book
  attr_reader :title

  def title=(new_title)
    conjunctions = ["for", "and", "nor", "but", "or", "yet", "so"]
    articles = ["a", "an", "the"]
    prepositions = ["of", "in"]
    no_capitalize = conjunctions + articles + prepositions
    cap_words = []

    words = new_title.split(" ")
    words.each_with_index do |word, i|
      if no_capitalize.include?(word) && i != 0
        cap_words << word
      else
        cap_words << word.capitalize
      end
    end
    @title = cap_words.join(" ")
  end
end
