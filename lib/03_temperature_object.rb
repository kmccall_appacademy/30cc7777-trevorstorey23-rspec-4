class Temperature

  def initialize(options={})
    @options = options
  end

  def in_fahrenheit
    @options.key?(:f) ? @options[:f] : Temperature.ctof(@options[:c])
  end

  def in_celsius
    @options.key?(:c) ? @options[:c] : Temperature.ftoc(@options[:f])
  end

  def self.from_celsius(temp)
    self.new({:c => temp})
  end

  def self.from_fahrenheit(temp)
    self.new({:f => temp})
  end

  def self.ctof(temp)
    temp * (9.0/5.0) + 32
  end

  def self.ftoc(temp)
    (temp - 32) * (5.0/9.0)
  end

end

class Celsius < Temperature
  def initialize(temp)
    @options = {:c => temp}
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @options = {:f => temp}
  end
end
