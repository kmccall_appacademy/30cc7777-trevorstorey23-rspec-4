class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    @entries.merge!(entry) if entry.is_a? Hash
    @entries.merge!(entry => nil) if entry.is_a? String
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    keywords.include? keyword
  end

  def find(prefix)
    found = {}
    found.merge!(@entries.select {|key, value| key =~ /#{prefix}/})
    found
  end

  def printable
    print_arr = []
    @entries.sort.each do |key, value|
      print_arr << "[#{key}] \"#{value}\""
    end
    print_arr.join("\n")
  end
end
